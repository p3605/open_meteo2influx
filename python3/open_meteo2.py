#!/usr/bin/python3
# -*- coding: utf-8 -*-
# copyright: pf@nc-x.com mit MIT-Lizenz
# in crontab einbinden und um 00:30 Uhr laufen lassen
# nutzt die open_meteo api und trägt die stündlich zu erwartenden
# Werte für solar-radiation und Wolkendecke ein.

import json
import urllib.request
import requests
import datetime
from datetime import date
from datetime import timedelta
from influxdb import InfluxDBClient

#configure position
lat='47.7'
lon='8.07'

#configure payload
#payload="cloudcover,shortwave_radiation,rain,windspeed_10m"
payload="cloudcover,shortwave_radiation"

# Configure InfluxDB connection variables Datenbank-Rechner
host = "127.0.0.1"
port = 8086
user = "wetter"
password = "open"
dbname = "openmeteo"

# Influx Datenbank verbinden
client = InfluxDBClient(host, port, user, password, dbname)

# Stundenwerte vom open_meteo Server einholen
def getDataFromServer():
    try:
        today=(date.today())
        # mit +/- timedelta können andere Tage angesprochen werden -1:gestern, +2: übermorgen
        #today = today + timedelta(days = 1)
        req = requests.get('https://api.open-meteo.com/v1/forecast?latitude='+lat+'&longitude='+lon+'&hourly='+payload+'&start_date='+str(today)+'&end_date='+str(today))
        if req.status_code != requests.codes.ok: return 0
        return req.json()
    except Exception as e:
        print(str(e))
        return 0

# Werte in die Influx-Datenbank eintragen
def writeDataToInfluxDB(stunde,name,wert):
    info=[{"measurement": "open_meteo", "time":stunde, "fields":{name : wert}}]
    #print(info)
    client.write_points(info, time_precision='m')
    return


# main 
out = getDataFromServer()
# das ganze json erstmal reduzieren
data=out['hourly']
for key in data.keys():
    # hier stehen die einzelnen Datenreihen nach 'hourly=' aus der api url
    # print(key, len(data[key])) # welche Datenreihe mit wievielen Datensätzen?
    if not key=='time':        # time wird zu jeder Datenreihe extra ausgegeben 
        i=0
        while i < len(data[key]): # für alle Datensätze/Paarungen
            # flexibles eintragen in die Influx-Datenbank unabhängig welche und wieviele Datenreihen abgefragt wurden
            writeDataToInfluxDB(data['time'][i],key,data[key][i]) 
            i +=1

# fertig
