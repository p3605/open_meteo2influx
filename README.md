## Name
# open_meteo2influx

## Description
uses provided api from open_meteo.com to collect hourly weather data. Historic data or as prediction.
Specially want to be able to compare my production of Balkonkraftwerk / Steckersolar in conjunction with clouds and possible production as radiation in w/m².

## Visuals
a dashboard example using influxDB and Grafana
![dashboard](/media/open_meteo2influx_grafana-example.png "radiation in Grafana dashboard")

## Installation
- download the python3 script, configure latitude and longitude. 
- If you want to have more variables from the api as cloud and radiation change the payload string variable. 
- Configure your influxDB client parameter.
- Create a database in influxDB with provided parameters.

## Usage
run the script once in the early morning from crontab to get prediction for this day.
Import your hourly values in your grafana timeseries.

## Support
may be availble from pf@nc-x.com

## Roadmap
improve README.MD

## Contributing
improvements of python3 code are very welcome.

## License
MIT 

## Project status
just discovered the api and did a quick'n'dirty coding. 
